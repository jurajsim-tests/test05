# test05

Test05, features:
- with first request it gets data from [cnb](http://www.cnb.cz/en/financial-markets/foreign-exchange-market/central-bank-exchange-rate-fixing/central-bank-exchange-rate-fixing/daily.txt) masked under /api/rates
- I didn't use react-query lib before but is quite nice 
- yes I know create react app is not the best, on prod level app I'd configure webpack manually or maybe even try to use something faster. Here I had no time to play with that. 
- contains unit tests (for some fns & cmps)
- contains e2e tests (cypress) for 2 scenarios only, didn't have time for more...
- published **live** on digital ocean https://test5-mskze.ondigitalocean.app/
- components would probably be more granular on production level app to be more testable
- normally I'd do some showcase or storybook for components but there is no need for this here
- validation is not great here, I should have use probably formik or something similar but that would be probably overkill
- prod level app would be visually very different probably (no mui & something lighter + scss), but here I was a bit lazy and a bit under time pressure, so I reused as many components/libs as possible
- prod level app would have some polyfills maybe, depends on supported browsers
- there was no need for nodejs BE, but I could use it instead of nginx. Some simple express would do.
- merry Christmas!

# Is it alive?

It's alive! ...on https://test5-mskze.ondigitalocean.app/ (digital ocean droplet)

# Running locally etc.

## devserver
```powershell
# install deps if necessary
npm i
# start the dev server
npm start
```

## docker

Build [local image](Dockerfile) and run it:

```powershell
docker build -t test5 --no-cache .
docker run -p 8080:8080 test5
```
under the hood it runs nginx which [proxy the data](./config/nginx/nginx.conf) from cnb and makes them available to /api/rates:

```
location /api/rates {
    rewrite /api/rates /en/financial-markets/foreign-exchange-market/central-bank-exchange-rate-fixing/central-bank-exchange-rate-fixing/daily.txt break;
    proxy_pass http://www.cnb.cz;
    proxy_cache_key rates;
    proxy_cache_valid 200 10m;
    proxy_cache_use_stale error timeout http_500 http_502 http_503 http_504 http_429;
    proxy_cache_lock on;
}
```
Note: just an example, would need to tune that cache on prod or cache somewhere else (varnish maybe)

(note: same thing with local dev server, only there local nodejs proxy does the thing)

pushing to dockerhub
```powershell
docker tag test5 jurajsim/test5:dev
docker push jurajsim/test5:dev
```

## tests

### unit

just a few cases covered. Run it locally via

```powershell
npm run test
```

### e2e (cypress)

just a few cases covered. Run it locally via
(it need local dev server running)

```powershell
npm start
npm run cypress
```

## other

Run local inter. Normally it would be bound to commit/push but not here...

```powershell
npm run lint
```

