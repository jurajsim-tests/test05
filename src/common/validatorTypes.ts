export interface IErrorBag {
    [key: string]: string;
}

export interface IValid {
    errors: IErrorBag;
    valid: boolean;
}
