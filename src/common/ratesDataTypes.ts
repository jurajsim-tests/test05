export interface IConversionRate {
    rate: number;
    country: string;
    code: string;
}
