import { IConversionRate } from './ratesDataTypes';
import { IErrorBag } from './validatorTypes';

export const validateAll = (currentRate: IConversionRate | undefined, czkAmount: string | undefined) => {
    const errors: IErrorBag = {};
    let valid = true;
    if (!currentRate) {
        errors['currentRate'] = 'Currency is required';
        valid = false;
    }
    if (czkAmount === '' || czkAmount === undefined) {
        errors['czkInput'] = 'Amount is required';
        valid = false;
    }
    if (!!czkAmount && isNaN(parseInt(czkAmount))) {
        errors['czkInput'] = 'Amount must be number';
        valid = false;
    }
    return { valid, errors };
};
