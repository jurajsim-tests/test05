import { IConversionRate } from './ratesDataTypes';

export const parseTextData = (data: string): Array<IConversionRate> => {
    const lines = data.split('\n');
    const rates: Array<IConversionRate> = [];
    for (let i = 2; i < lines.length; i++) {
        const row = lines[i].split('|');
        if (row.length !== 5) {
            continue;
        }
        rates.push({
            country: row[0],
            code: row[3],
            rate: parseFloat(row[4]),
        });
    }
    return rates;
};
