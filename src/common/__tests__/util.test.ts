import { waitFor } from '../util';

it('waits for one second', () => {
    jest.useFakeTimers();
    jest.spyOn(global, 'setTimeout');
    const result = waitFor('1s');
    jest.advanceTimersByTime(1000);
    expect(result).resolves.toBe(undefined);
    expect(setTimeout).toHaveBeenCalledTimes(1);
    expect(setTimeout).toHaveBeenLastCalledWith(expect.any(Function), 1000);
    jest.useRealTimers();
});
