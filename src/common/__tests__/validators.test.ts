import { IConversionRate } from '../ratesDataTypes';
import { validateAll } from '../validators';

describe('validate two values', () => {
    it('validates and all are valid', () => {
        const rate: IConversionRate = {
            rate: 2,
            code: 'TST',
            country: 'Test country',
        };
        expect(validateAll(rate, '250')).toEqual({
            errors: {},
            valid: true,
        });
    });
    it('fails because rate is not set', () => {
        expect(validateAll(undefined, '250')).toEqual({
            errors: { currentRate: 'Currency is required' },
            valid: false,
        });
    });
    it('fails because czk amount is not set', () => {
        const rate: IConversionRate = {
            rate: 2,
            code: 'TST',
            country: 'Test country',
        };
        expect(validateAll(rate, undefined)).toEqual({
            errors: { czkInput: 'Amount is required' },
            valid: false,
        });
    });
    it('fails because czk amount is invalid', () => {
        const rate: IConversionRate = {
            rate: 2,
            code: 'TST',
            country: 'Test country',
        };
        expect(validateAll(rate, 'notValidFloat')).toEqual({
            errors: { czkInput: 'Amount must be number' },
            valid: false,
        });
    });
    it('fails because nothing is set', () => {
        expect(validateAll(undefined, undefined)).toEqual({
            errors: { czkInput: 'Amount is required', currentRate: 'Currency is required' },
            valid: false,
        });
    });
});
