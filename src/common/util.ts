import ms from 'ms';

export const waitFor = (amountOfTime: string): Promise<void> => {
    return new Promise((resolve) => {
        setTimeout(() => {
            return resolve();
        }, ms(amountOfTime));
    });
};
