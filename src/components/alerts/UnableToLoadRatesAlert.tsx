import { Alert, Button } from '@mui/material';
import React from 'react';

interface IUnableToLoadRatesAlert {
    onTryAgainClicked: () => void;
    errorMessage: string;
}

export const UnableToLoadRatesAlert: React.FC<IUnableToLoadRatesAlert> = ({ onTryAgainClicked, errorMessage }) => {
    return (
        <Alert color="error">
            Unable to download currency rates: {errorMessage}&nbsp;
            <Button className="t_try-again-button" variant="outlined" size="small" onClick={onTryAgainClicked}>
                Try again
            </Button>
        </Alert>
    );
};
