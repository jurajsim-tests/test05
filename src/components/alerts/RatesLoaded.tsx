import { Alert, Box, Button } from '@mui/material';
import React from 'react';

interface IRatesLoadedProps {
    onLoadAgainClicked: () => void;
}

export const RatesLoadedAlert: React.FC<IRatesLoadedProps> = ({ onLoadAgainClicked }) => {
    return (
        <Alert className="t_rates-loaded-alert" color="success">
            <Box alignItems="center" sx={{ padding: 0 }}>
                Currency rates successfully loaded.{' '}
                <Button variant="outlined" size="small" onClick={onLoadAgainClicked}>
                    Load again
                </Button>
            </Box>
        </Alert>
    );
};
