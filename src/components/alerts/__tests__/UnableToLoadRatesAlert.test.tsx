import { fireEvent, render, screen } from '@testing-library/react';
import React from 'react';

import { UnableToLoadRatesAlert } from '../UnableToLoadRatesAlert';

it('renders alert and click on button', () => {
    const cb = jest.fn();
    render(<UnableToLoadRatesAlert onTryAgainClicked={cb} errorMessage="test message" />);
    const linkElement = screen.getByText(/test message/i);
    expect(linkElement).toBeInTheDocument();
    const buttonElement = document.querySelector('.t_try-again-button');
    expect(buttonElement).toBeInTheDocument();
    fireEvent.click(buttonElement!);
    expect(cb).toBeCalledTimes(1);
});
