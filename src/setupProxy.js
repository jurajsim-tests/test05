// eslint-disable-next-line @typescript-eslint/no-var-requires,no-undef
const { createProxyMiddleware } = require('http-proxy-middleware');

// eslint-disable-next-line no-undef
module.exports = function (app) {
    app.use(
        '/api/rates',
        createProxyMiddleware({
            target: 'http://www.cnb.cz',
            changeOrigin: true,
            pathRewrite: {
                '^/api/rates':
                    '/cs/financni-trhy/devizovy-trh/kurzy-devizoveho-trhu/kurzy-devizoveho-trhu/denni_kurz.txt',
            },
        }),
    );
};
