import './App.css';

import { Alert, Autocomplete, Button, Grid, InputAdornment, TextField, Typography } from '@mui/material';
import React, { ChangeEvent, SyntheticEvent, useEffect, useRef, useState } from 'react';
import { useQuery } from 'react-query';

import { parseTextData } from './common/ratesData';
import { IConversionRate } from './common/ratesDataTypes';
import { waitFor } from './common/util';
import { validateAll } from './common/validators';
import { IErrorBag, IValid } from './common/validatorTypes';
import { RatesLoadedAlert } from './components/alerts/RatesLoaded';
import { UnableToLoadRatesAlert } from './components/alerts/UnableToLoadRatesAlert';

function App() {
    // const [foreignAmount, setForeignAmount] = useState<number | undefined>();
    const [currentRate, setCurrentRate] = useState<IConversionRate | undefined>();
    const [czkAmount, setCzkAmount] = useState<string | undefined>();
    const [errorBag, setErrorBag] = useState<IErrorBag | undefined>();
    const [result, setResult] = useState<string | undefined>();
    //yea this is not quite good
    const [validationEnabled, setValidationEnabled] = useState<boolean>(false);

    const czkInput = useRef(undefined);

    const validateForm = (): IValid => {
        if (!validationEnabled) {
            setValidationEnabled(true);
        }
        const { errors, valid } = validateAll(currentRate, czkAmount);
        setErrorBag(errors);
        return { valid, errors };
    };

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const {
        isError,
        data: conversionRates,
        error,
        refetch,
        isFetching,
    } = useQuery<Array<IConversionRate>, Error>('getConversionRates', async () => {
        const response = await fetch('api/rates');
        if (!response.ok) {
            throw new Error(`Conversion rates fetch failed with code ${response.status}`, {
                cause: response.status,
            });
        }
        //just for the show...
        await waitFor('1s');
        return parseTextData(await response.text());
    });

    const onConvertBtnClick = () => {
        const { valid } = validateForm();

        if (valid && !!czkAmount && currentRate) {
            const res = parseFloat(czkAmount) / currentRate.rate;
            setResult(
                `${+parseFloat(czkAmount).toFixed(2)} CZK is ${res.toFixed(2)} ${currentRate.code.toUpperCase()}`,
            );
        }
    };

    const onCzkAmountChange = (evt: ChangeEvent<HTMLInputElement>) => {
        setCzkAmount(evt.target.value);
    };

    const onCurrencyRateChange = (evt: SyntheticEvent<Element, Event>, value: IConversionRate | null) => {
        if (!value) {
            setCurrentRate(undefined);
            return;
        }
        setCurrentRate(value);
    };

    useEffect(() => {
        if (conversionRates && validationEnabled) {
            validateForm();
        }
    }, [currentRate, czkAmount, validationEnabled]);

    return (
        <div className="App">
            <Grid container spacing={2} direction="column" padding={2}>
                <Grid item>
                    <Grid container spacing={2} direction="column" padding={2}>
                        <Grid item>
                            <Grid container spacing={2} justifyContent="center" alignItems="center">
                                <Grid item>
                                    <TextField
                                        inputRef={czkInput}
                                        className="t_czk-amount"
                                        id="outlined-basic"
                                        label="Amount"
                                        error={!!errorBag?.['czkInput']}
                                        helperText={errorBag?.['czkInput']}
                                        type="number"
                                        onChange={onCzkAmountChange}
                                        placeholder="Enter amount..."
                                        variant="outlined"
                                        InputProps={{
                                            endAdornment: <InputAdornment position="end">CZK</InputAdornment>,
                                        }}
                                    />
                                </Grid>
                                <Grid item>
                                    <Typography>convert to</Typography>
                                </Grid>
                                <Grid item>
                                    {!!conversionRates && (
                                        <Autocomplete
                                            disabled={isFetching || !conversionRates}
                                            onChange={onCurrencyRateChange}
                                            autoHighlight
                                            getOptionLabel={(option) => option.code}
                                            options={conversionRates}
                                            renderInput={(params) => (
                                                <TextField
                                                    {...params}
                                                    label="Currency"
                                                    fullWidth={false}
                                                    className="t_currency-rate"
                                                    error={!!errorBag?.['currentRate']}
                                                    helperText={errorBag?.['currentRate']}
                                                    sx={{ minWidth: 150 }}
                                                    placeholder="Select currency..."
                                                    inputProps={{
                                                        ...params.inputProps,
                                                        autoComplete: 'new-password', // disable autocomplete and autofill
                                                    }}
                                                />
                                            )}
                                        ></Autocomplete>
                                    )}
                                </Grid>
                                <Grid item>
                                    <Button
                                        disabled={isFetching || !conversionRates}
                                        size="large"
                                        className="t_convert-button"
                                        onClick={onConvertBtnClick}
                                        variant="outlined"
                                    >
                                        Convert
                                    </Button>
                                </Grid>
                            </Grid>
                        </Grid>
                        <Grid item>{result && <Typography className="t_result">{result}</Typography>}</Grid>
                    </Grid>
                </Grid>
                <Grid item>
                    <Grid container spacing={2} direction="column">
                        {isFetching && (
                            <Grid item>
                                <Alert color="info">Loading currency rates...</Alert>
                            </Grid>
                        )}
                        {!isFetching && isError && error && (
                            <Grid item>
                                <UnableToLoadRatesAlert
                                    onTryAgainClicked={refetch}
                                    errorMessage={error.message}
                                ></UnableToLoadRatesAlert>
                            </Grid>
                        )}
                        {!isFetching && !isError && (
                            <Grid item>
                                <RatesLoadedAlert onLoadAgainClicked={refetch}></RatesLoadedAlert>
                            </Grid>
                        )}
                    </Grid>
                </Grid>
            </Grid>
        </div>
    );
}

export default App;
