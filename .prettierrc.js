module.exports = {
    semi: true,
    trailingComma: 'all',
    bracketSpacing: true,
    singleQuote: true,
    jsxSingleQuote: false,
    arrowParens: 'always',
    printWidth: 120,
    tabWidth: 4,
    useTabs: false,
};
