# base image
FROM node:18-alpine as build

# set working directory
WORKDIR /app

# add `/app/node_modules/.bin` to $PATH
ENV PATH /app/node_modules/.bin:$PATH

COPY . /app

ENV CI true
RUN npm config set unsafe-perm true && \
    npm config set progress false && \
    npm config set fund false && \
    npm ci && \
    npm run build

FROM nginx:1.21.5-alpine AS base

COPY --from=build /app/build /usr/share/nginx/html

LABEL org.label-schema.version=$BUILD_VERSION
LABEL org.label-schema.schema-version="1.0"

COPY config/nginx/nginx.conf /etc/nginx

CMD ["nginx"]
