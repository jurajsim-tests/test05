describe('convert some amount', () => {
    it('very basic positive scenario', () => {
        cy.visit('http://localhost:3000');
        cy.get('.t_rates-loaded-alert', { timeout: 10000 }).should('be.visible');
        cy.get('.t_czk-amount input').type(50);
        cy.get('.t_currency-rate input').type('EUR{enter}');
        cy.wait(1000);
        cy.get('.t_convert-button').click();
        cy.wait(500);
        cy.get('.t_result', { timeout: 1000 })
            .should('be.visible')
            .contains(/50\sCZK\sis\s[0-9]+(\.[0-9]{0,2})?\sEUR/);
    });

    it('should not display result if amount is missing', () => {
        cy.visit('http://localhost:3000');
        cy.get('.t_rates-loaded-alert', { timeout: 10000 }).should('be.visible');
        cy.get('.t_currency-rate input').type('EUR{enter}');
        cy.wait(1000);
        cy.get('.t_convert-button').click();
        cy.get('.t_result').should('not.exist');
        cy.contains('Amount is required');
    });

    ///etc the rest...
});
